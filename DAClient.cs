﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityModel;
using sytem.Configuration;

namespace DataAccessLayer
{
    public class DAClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<SqlParameter> parameters = null;
        private SqlParameter parameter = null;
        public DataTable GetClients(string cscid, string orwid)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {

                sqlHelper.CreateObjects(true);

                parameters = new List<SqlParameter>();

                parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 15);
                parameter.Value = cscid;
                parameters.Add(parameter);

                parameter = new SqlParameter("@ORWID", SqlDbType.VarChar, 15);
                parameter.Value = orwid;
                parameters.Add(parameter);
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].GetClients", parameters);

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DAClient-GetClients()" + ex.Message);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;

        }
        public DataTable GetClients(string cscid)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {

                sqlHelper.CreateObjects(true);

                parameters = new List<SqlParameter>();

                parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 15);
                parameter.Value = cscid;
                parameters.Add(parameter);
              
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].GetClientsbyCSC", parameters);

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DAClient-GetClientsbyCSC()" + ex.Message);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;

        }

        public bool updateClient(List<Client> clients)
        {
            bool Successfull = false;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {                
                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                foreach (Client client in clients)
                {
                    parameters.Clear();
                    parameter = new SqlParameter("@CLIENTID", SqlDbType.VarChar, 20);
                    parameter.Value = client.ClientID;
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 15);
                    parameter.Value = client.CSCID;
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@ORWID", SqlDbType.VarChar, 15);
                    parameter.Value = client.ORWID;
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@Name", SqlDbType.VarChar, 100);
                    if (client.Name != null)
                    {
                        parameter.Value = client.Name;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@ADDRESS", SqlDbType.VarChar, 200);
                    if (client.ADDRESS != null)
                    {
                        parameter.Value = client.ADDRESS;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Village", SqlDbType.VarChar, 200);
                    if (client.Village != null)
                    {
                        parameter.Value = client.Village;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Mandal", SqlDbType.VarChar, 200);
                    if (client.Mandal != null)
                    {
                        parameter.Value = client.Mandal;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@DoorNo", SqlDbType.VarChar, 50);
                    if (client.DoorNo != null)
                    {
                        parameter.Value = client.DoorNo;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@HouseName", SqlDbType.VarChar, 100);
                    if (client.HouseName != null)
                    {
                        parameter.Value = client.HouseName;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Landmark", SqlDbType.VarChar, 100);
                    if (client.Landmark != null)
                    {
                        parameter.Value = client.Landmark;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@StateID", SqlDbType.Int);
                    if (client.StateID != null)
                    {
                        parameter.Value = client.StateID;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@District", SqlDbType.Int);
                    if (client.District != null)
                    {
                        parameter.Value = client.District_Code;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@PinCode", SqlDbType.VarChar,10);
                    if (client.District != null)
                    {
                        parameter.Value = client.PinCode;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@ContactNumber", SqlDbType.VarChar, 20);
                    if (client.ContactNumber != null)
                    {
                        parameter.Value = client.ContactNumber;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@AGE", SqlDbType.Int);
                    if (client.AGE != null)
                    {
                        parameter.Value = client.AGE;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@Gender", SqlDbType.TinyInt);
                    if (!string.IsNullOrEmpty(client.Gender.ToString()))
                    {
                        parameter.Value = client.Gender;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@FamilyID", SqlDbType.VarChar, 20);
                    if (client.FamilyID != null)
                    {
                        parameter.Value = client.FamilyID;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@PHOTOURL", SqlDbType.VarChar);
                    if (client.PHOTOURL != null)
                    {
                        parameter.Value = client.PHOTOURL;
                    }
                    else
                    {
                        parameter.Value = "";
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@RegnDate", SqlDbType.Date);
                    if (client.RegnDate != null)
                    {
                        parameter.Value = client.RegnDate;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }              
                   
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@NumFamilyMembers", SqlDbType.Int);
                    if (!string.IsNullOrEmpty(client.NumFamilyMembers.ToString()))
                    {
                        parameter.Value = client.NumFamilyMembers;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@PREGNANTLINKTOPPTCT", SqlDbType.Int);
                    if (!string.IsNullOrEmpty(client.PREGNANTLINKTOPPTCT.ToString()))
                    {
                        parameter.Value = client.PREGNANTLINKTOPPTCT;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@ORPHANSTATUS", SqlDbType.TinyInt);
                    if (!string.IsNullOrEmpty(client.ORPHANSTATUS.ToString()))
                    {
                        parameter.Value = client.ORPHANSTATUS;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@WILLINGFORSOCIALSCHEMES", SqlDbType.Int);
                    if (!string.IsNullOrEmpty(client.WILLINGFORSOCIALSCHEMES.ToString()))
                    {
                        parameter.Value = client.WILLINGFORSOCIALSCHEMES;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }

                    parameters.Add(parameter);

                    parameter = new SqlParameter("@LATITUDE", SqlDbType.VarChar, 30);
                    if (client.LATITUDE != null)
                    {
                        parameter.Value = client.LATITUDE;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@LANGITUDE", SqlDbType.VarChar, 30);
                    if (client.LANGITUDE != null)
                    {
                        parameter.Value = client.LANGITUDE;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@CREATEDDATE", SqlDbType.Date);
                    if (client.CREATEDDATE != null)
                    {
                        parameter.Value = client.CREATEDDATE;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);

                    parameter = new SqlParameter("@ARTSTATUS", SqlDbType.TinyInt);
                    if (client.ARTSTATUS != null)
                    {
                        parameter.Value = client.ARTSTATUS;
                        parameters.Add(parameter);
                    }                 
                    

                    parameter = new SqlParameter("@DATEOFBIRTH", SqlDbType.Date);
                    if (client.DATEOFBIRTH != null)
                    {
                        parameter.Value = client.DATEOFBIRTH;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@DATEOFBIRTHFORMATTED", SqlDbType.VarChar, 50);
                    if (client.DATEOFBIRTHFORMATTED != null)
                    {
                        parameter.Value = client.DATEOFBIRTHFORMATTED;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@Education", SqlDbType.TinyInt);
                    if (!string.IsNullOrEmpty(client.Education.ToString()))
                    {
                        parameter.Value = client.Education;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@WILLINGNESSDETAILS", SqlDbType.VarChar);
                    if (client.WILLINGNESSDETAILS != null)
                    {
                        parameter.Value = client.WILLINGNESSDETAILS;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@OCCUPATION", SqlDbType.TinyInt);
                    if (!string.IsNullOrEmpty(client.OCCUPATION.ToString()))
                    {
                        parameter.Value = client.OCCUPATION;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@MONTHLYINCOME", SqlDbType.Int);
                    if (!string.IsNullOrEmpty(client.MONTHLYINCOME.ToString()))
                    {
                        parameter.Value = client.MONTHLYINCOME;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@fldSysCRFID", SqlDbType.VarChar, 15);
                    if (client.fldSysCRFID != null)
                    {
                        parameter.Value = client.fldSysCRFID;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@fldCreatedBy", SqlDbType.VarChar, 50);
                    if (client.fldCreatedBy != null)
                    {
                        parameter.Value = client.fldCreatedBy;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@DateHIVdetection ", SqlDbType.DateTime);
                    if (client.DateHIVdetection != null && client.DateHIVdetection != DateTime.MinValue)
                    {
                        parameter.Value = client.DateHIVdetection;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);

                    parameter = new SqlParameter("@WhetherclientbelongsBPLfamily", SqlDbType.BigInt);
                    if (client.WhetherclientbelongsBPLfamily != null)
                    {
                        parameter.Value = client.WhetherclientbelongsBPLfamily;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@DateCD4testing1", SqlDbType.DateTime);
                    if (client.DateCD4testing1 != null)
                    {
                        parameter.Value = client.DateCD4testing1;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@DateCD4testing2", SqlDbType.DateTime);
                    if (client.DateCD4testing2 != null)
                    {
                        parameter.Value = client.DateCD4testing2;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@DateCD4testing3", SqlDbType.DateTime);
                    if (client.DateCD4testing3 != null)
                    {
                        parameter.Value = client.DateCD4testing3;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@CD4count1", SqlDbType.VarChar, 20);
                    if (client.CD4count1 != null)
                    {
                        parameter.Value = client.CD4count1;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@CD4count2", SqlDbType.VarChar, 20);
                    if (client.CD4count2 != null)
                    {
                        parameter.Value = client.CD4count2;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@CD4count3", SqlDbType.VarChar, 20);
                    if (client.CD4count3 != null)
                    {
                        parameter.Value = client.CD4count3;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@ARTregimentype", SqlDbType.BigInt);
                    if (client.ARTregimentype != null)
                    {
                        parameter.Value = client.ARTregimentype;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Weight", SqlDbType.Float);
                    if (client.ARTregimentype != null)
                    {
                        parameter.Value = client.ARTregimentype;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Height", SqlDbType.Float);
                    if (client.Height != null)
                    {
                        parameter.Value = client.Height;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@WelfareSchemelinkedto", SqlDbType.VarChar, 20);
                    if (client.WelfareSchemelinkedto != null)
                    {
                        parameter.Value = client.WelfareSchemelinkedto;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Referredfrom", SqlDbType.Int);
                    if (client.Referredfrom != null)
                    {
                        parameter.Value = client.Referredfrom;
                        parameters.Add(parameter);
                    }
                    
                    
                    parameter = new SqlParameter("@Infantfeeding", SqlDbType.Int);
                    if (client.Infantfeeding != null)
                    {
                        parameter.Value = client.Infantfeeding;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@RiskFactor", SqlDbType.VarChar,50);
                    if (client.RiskFactor != null)
                    {
                        parameter.Value = client.RiskFactor;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@IDUInformation", SqlDbType.Int);
                    if (client.IDUInformation != null)
                    {
                        parameter.Value = client.IDUInformation;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@SEARSupport", SqlDbType.VarChar,200);
                    if (client.SEARSupport != null)
                    {
                        parameter.Value = client.SEARSupport;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@DateCD4testing ", SqlDbType.DateTime);
                    if (client.DateCD4testing != null)
                    {
                        parameter.Value = client.DateCD4testing;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Dateoffolloawupvisit ", SqlDbType.DateTime);
                    if (client.Dateoffolloawupvisit != null)
                    {
                        parameter.Value = client.Dateoffolloawupvisit;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@CD4COUNT", SqlDbType.VarChar, 200);
                    if (client.SEARSupport != null)
                    {
                        parameter.Value = client.CD4COUNT;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Aadharnumber", SqlDbType.VarChar, 20);
                    if (client.Aadharnumber != null)
                    {
                        parameter.Value = client.Aadharnumber;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@PreART", SqlDbType.VarChar, 50);
                    if (client.PreART != null)
                    {
                        parameter.Value = client.PreART;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@ART", SqlDbType.VarChar, 50);
                    if (client.ART != null)
                    {
                        parameter.Value = client.ART;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Latest_DateCD4testing ", SqlDbType.DateTime);
                    if (client.Latest_DateCD4testing != null)
                    {
                        parameter.Value = client.Latest_DateCD4testing;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@Latest_CD4count", SqlDbType.VarChar, 6);
                    if (client.Latest_CD4count != null)
                    {
                        parameter.Value = client.Latest_CD4count;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@TBDateScreening ", SqlDbType.DateTime);
                    if (client.TBDateScreening != null)
                    {
                        parameter.Value = client.TBDateScreening;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@EDD ", SqlDbType.DateTime);
                    if (client.EDD != null)
                    {
                        parameter.Value = client.EDD;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@LMP ", SqlDbType.DateTime);
                    if (client.LMP != null)
                    {
                        parameter.Value = client.LMP;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);
                                        
                    parameter = new SqlParameter("@TRIMESTER", SqlDbType.Int);
                    if (client.TRIMESTER != null)
                    {
                        parameter.Value = client.TRIMESTER;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@PREGNANTCD4COUNT", SqlDbType.Int);
                    if (client.PREGNANTCD4COUNT != null)
                    {
                        parameter.Value = client.PREGNANTCD4COUNT;
                    }
                    else
                    {
                        parameter.Value = 0;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@PREGANANTART", SqlDbType.Int);
                    if (client.PREGANANTART != null)
                    {
                        parameter.Value = client.PREGANANTART;
                        parameters.Add(parameter);
                    }
                    
                    if (client.INSTIDELIVERY != null)
                    {
                        parameter = new SqlParameter("@INSTIDELIVERY", SqlDbType.Int);
                        parameter.Value = client.INSTIDELIVERY;
                        parameters.Add(parameter);
                    }
                    if (client.NEVIRAPINGIVEN != null)
                    {
                        parameter = new SqlParameter("@NEVIRAPINGIVEN", SqlDbType.Int);
                        parameter.Value = client.NEVIRAPINGIVEN;
                        parameters.Add(parameter);
                    }
                    if (client.CHILDDETAILS != null)
                    {
                        parameter = new SqlParameter("@CHILDDETAILS", SqlDbType.Int);
                        parameter.Value = client.CHILDDETAILS;
                        parameters.Add(parameter);
                    }
                    if (client.EIDSDBC1 != null)
                    {
                        parameter = new SqlParameter("@EIDSDBC1", SqlDbType.Int);
                        parameter.Value = client.EIDSDBC1;
                        parameters.Add(parameter);
                    }
                    if (client.EIDSDBC2 != null)
                    {
                        parameter = new SqlParameter("@EIDSDBC2", SqlDbType.Int);
                        parameter.Value = client.EIDSDBC2;
                        parameters.Add(parameter);
                    }
                    if (client.CHILDHIVSTATUS != null)
                    {
                        parameter = new SqlParameter("@CHILDHIVSTATUS", SqlDbType.Int);
                        parameter.Value = client.CHILDHIVSTATUS;
                        parameters.Add(parameter);
                    }
                    parameter = new SqlParameter("@PREGANANTUPDATEDATE ", SqlDbType.DateTime);
                    if (client.PREGANANTUPDATEDATE != null && client.PREGANANTUPDATEDATE != DateTime.MinValue)
                    {
                        parameter.Value = client.PREGANANTUPDATEDATE;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);
                    parameter = new SqlParameter("@PRAGIDENDIFICATIONDATE ", SqlDbType.DateTime);
                    if (client.PRAGIDENDIFICATIONDATE != null && client.PRAGIDENDIFICATIONDATE != DateTime.MinValue)
                    {
                        parameter.Value = client.PRAGIDENDIFICATIONDATE;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }

                    parameters.Add(parameter);

                    Successfull = sqlHelper.SQLHelper_ExecuteNonQuery("[dbo].updateClient", parameters);

                }                
                if(!Successfull)
                {
                    sqlHelper.RollBackTransction();
                }
                else
                {
                    sqlHelper.CommitTransction();
                }
            }
            catch (Exception ex)
            {
                sqlHelper.RollBackTransction();
                logger.Error("DataAccess-DAClient-UpdateClient()" + ex.Message);
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return Successfull;
        }
    }
}
