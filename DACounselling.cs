﻿using EntityModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class DACounselling
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<SqlParameter> parameters = null;
        private SqlParameter parameter = null;
        public bool updateCounsellingDetails(List<CousellingDetail> counsellingDetails)
        {
            bool Successfull = false;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {
                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                foreach (CousellingDetail counselling in counsellingDetails)
                {
                    parameters.Clear();
                    parameter = new SqlParameter("@CLIENTID", SqlDbType.VarChar, 20);
                    parameter.Value = counselling.CLIENTID;
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 15);
                    parameter.Value = counselling.CSCID;
                    parameters.Add(parameter);

                    parameter = new SqlParameter("@ORWID", SqlDbType.VarChar, 15);
                    parameter.Value = counselling.ORWID;
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@COUNSDATE", SqlDbType.DateTime);
                    if (counselling.COUNSDATE != null)
                    {
                        parameter.Value = counselling.COUNSDATE;
                    }
                    else
                    {
                        parameter.Value = "1/1/9999";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@FOLLOWTYPE", SqlDbType.VarChar, 200);
                    if (counselling.FOLLOWTYPE != null)
                    {
                        parameter.Value = counselling.FOLLOWTYPE;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@COUNSRECIVER", SqlDbType.VarChar, 200);
                    if (counselling.COUNSRECIVER != null)
                    {
                        parameter.Value = counselling.COUNSRECIVER;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@COUNSTYPE", SqlDbType.VarChar, 200);
                    if (counselling.COUNSTYPE != null)
                    {
                        parameter.Value = counselling.COUNSTYPE;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@NUMCONSOMSDIST", SqlDbType.Int);
                    if (counselling.NUMCONSOMSDIST != null)
                    {
                        parameter.Value = counselling.NUMCONSOMSDIST;
                    }
                    else
                    {
                        parameter.Value = DBNull.Value;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@REMARKS", SqlDbType.VarChar, 200);
                    if (counselling.REMARKS != null)
                    {
                        parameter.Value = counselling.REMARKS;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@COUNSELLEDON", SqlDbType.VarChar, 200);
                    if (counselling.COUNSELLEDON != null)
                    {
                        parameter.Value = counselling.COUNSELLEDON;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@COUNSDATEFORMATTED", SqlDbType.VarChar, 200);
                    if (counselling.COUNSDATEFORMATTED != null)
                    {
                        parameter.Value = counselling.COUNSDATEFORMATTED;
                    }
                    else
                    {
                        parameter.Value = "";
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@CREATEDDATE", SqlDbType.Date);
                    if (counselling.CREATEDDATE != null)
                    {
                        parameter.Value = counselling.CREATEDDATE;
                    }
                    else
                    {
                        parameter.Value = DateTime.MinValue;
                    }
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@MODIFIEDDATE", SqlDbType.Date);
                    if (counselling.MODIFIEDDATE != null)
                    {
                        parameter.Value = counselling.MODIFIEDDATE;
                    }
                    else
                    {
                        parameter.Value = DateTime.MinValue;
                    }
                    parameters.Add(parameter);
                    Successfull = sqlHelper.SQLHelper_ExecuteNonQuery("[dbo].updateCounsellingDetails", parameters);
                }
                sqlHelper.CommitTransction();
                if (!Successfull)
                {
                    sqlHelper.RollBackTransction();
                }
            }
            catch (Exception ex)
            {
                sqlHelper.RollBackTransction();
                logger.Error("DataAccess-DACounselling-UpdateCounsellingDetails()" + ex.Message);
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return Successfull;
        }

        public DataTable GetCounselling(string cscid, string orwid)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {

                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();

                parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 15);
                parameter.Value = cscid;
                parameters.Add(parameter);

                parameter = new SqlParameter("@ORWID", SqlDbType.VarChar, 15);
                parameter.Value = orwid;
                parameters.Add(parameter);
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].GetCounsellingDetails", parameters);

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DACounselling-GetCounsellingDetails()" + ex.Message);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;

        }
    }
}
