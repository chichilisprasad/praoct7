﻿using EntityModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DAAutenticate
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<SqlParameter> parameters = null;
        private SqlParameter parameter = null;
        public DataTable GetAuthenticateUser(string cscid, string orwid, string password)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {

                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 30);
                parameter.Value = cscid;
                parameters.Add(parameter);
                parameter = new SqlParameter("@ORWID", SqlDbType.VarChar, 30);
                parameter.Value = orwid;
                parameters.Add(parameter);
                parameter = new SqlParameter("@PASSWORD", SqlDbType.VarChar, 30);
                parameter.Value = password;
                parameters.Add(parameter);
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].getAuthUser", parameters);
                //logger.Error("DataAccess-DAUserInfo-GetUserInfo()" + sqlHelper.ConnectionValue);

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DAAutheticate-GetAuthenticateUser()" + ex.Message + sqlHelper.ConnectionValue);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;
        }
        public bool UpdateUser(List<ORWLLUpdate> oRWLLUpdates)
        {
            bool Successfull = false;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {
                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                foreach (ORWLLUpdate orwLLUpdate in oRWLLUpdates)
                {
                    parameters.Clear(); 
                    parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 15);
                    parameter.Value = orwLLUpdate.CSCID;
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@ORWID", SqlDbType.VarChar, 15);
                    parameter.Value = orwLLUpdate.ORWID;
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@LATITUDE", SqlDbType.VarChar, 15);
                    parameter.Value = orwLLUpdate.LATITUDE;
                    parameters.Add(parameter);
                    parameter = new SqlParameter("@LANGITUDE", SqlDbType.VarChar, 15);
                    parameter.Value = orwLLUpdate.LANGITUDE;
                    parameters.Add(parameter);
                    Successfull = sqlHelper.SQLHelper_ExecuteNonQuery("[dbo].UpdateLatlang", parameters);
                }              
            
            if (!Successfull)
            {
                sqlHelper.RollBackTransction();
            }
            else
            {
                sqlHelper.CommitTransction();
            }
            }
            catch (Exception ex)
            {
                sqlHelper.RollBackTransction();
                logger.Error("DataAccess-DAAuthenticate-UpdateUser()" + ex.Message);
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return Successfull;
        }
    }
}
