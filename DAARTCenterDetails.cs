﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DAARTCenterDetails
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<SqlParameter> parameters = null;
        private SqlParameter parameter = null;
        public DataTable GetARTCenterDistrictsByCSC(string CSCID)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {
                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                parameter = new SqlParameter("@CSCID", SqlDbType.VarChar, 30);
                parameter.Value = CSCID;
                parameters.Add(parameter);
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].GetARTCenterDistrictsByCSC", parameters);                

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DAARTCenterDetails-ARTCenterCodeDetails()" + ex.Message + sqlHelper.ConnectionValue);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;
        }

        public DataTable GetARTCenterDistrictsByState(string State)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {
                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                parameter = new SqlParameter("@STATE", SqlDbType.VarChar, 30);
                parameter.Value = State;
                parameters.Add(parameter);
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].GetARTCenterDistrictsByState", parameters);

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DAARTCenterDetails-GetARTCenterDistrictsByState()" + ex.Message + sqlHelper.ConnectionValue);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;
        }

        public DataTable GetARTCenterByDistrict(string district)
        {
            DataTable dt = null;
            SqlHelper1 sqlHelper = new SqlHelper1();
            try
            {
                sqlHelper.CreateObjects(true);
                parameters = new List<SqlParameter>();
                parameter = new SqlParameter("@DISTRICT", SqlDbType.VarChar, 30);
                parameter.Value = district;
                parameters.Add(parameter);
                dt = sqlHelper.SQLHelper_ExecuteReader("[dbo].GetARTCenterByDistrict", parameters);

            }
            catch (Exception ex)
            {
                logger.Error("DataAccess-DAARTCenterDetails-GetARTCenterByDistrict()" + ex.Message + sqlHelper.ConnectionValue);
                throw ex;
            }
            finally
            {
                sqlHelper.ClearObjects();
            }
            return dt;
        }
    }
}
